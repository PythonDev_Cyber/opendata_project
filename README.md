Hello =)

Here are the tools I've used : 
*Python 3.7 and it's built in packages(sqlite3,json...)
* OS : Windows 8.1
* the Requests Module -> pip install requests
* Pytest as a testing framework -> pip install pytest==2.9.1 

*The script can take a few seconds to run to completion

- part1.py -> the script for scraping the mentionned webpages/database creation and datasets download
    * 
- test_part1.py -> testing and verifying the results of part1.py using pytest

How to execute :
part1.py -> in CMD enter 
- python part1.py 

test_part1.py  -> to verify the results, enter the following in the console :
- py.test test_part1.py 


The execution of these scripts will create two folders : database and storage -> the first for keeping the database file,
the second for keeping the downloaded datasets



The folder part2 contains the rest of the exercise : 

For this part we need to install the following : 

*Flask as a lightweight web app which can be installed via :  pip install -U Flask,

*Jinja as a template language which can be installed via :  pip install Jinja2

It contains the following files : 

- part2.py is part1.py but without the output to the console.

- API_Miner.py is the API for listing files' metadata and downloading a specific dataset.

- web.py is a simple web app that user the API mentionned above

I chose not to add tests to this part as it's easier to test the API by integrating it in a simple web app: web.py
The folder template contains index.html, the file returned after every GET request.

After every POST request, the jinja variables are updated in index.html

- How to execute:  
launch web.py in CMD with :  python web.py
then in your browser enter : 127.0.0.1:5000

*The script can take a few seconds to run -> 127.0.0.1:5000 won't be accessible immediately
The metadata is displayed and underneath it are buttons for each file -> a python Tkinter dialog appears so that the user
can choose where to save his file



