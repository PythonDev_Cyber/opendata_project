import requests
import json
import sqlite3
import re
import os
import random

#part2.py is exactly like part1.py but without writing output to the console

'''
Given our url, we scrape the the pages ?page=3000 -> ?page= 3005 using requests
we use json to pretty print the json data we receive in return
We add the results to a dictionary for each URL 
'''	
def parseFunct():
	returnDict = {}
	for i in range(0,6):
		urlString = 'https://www.data.gouv.fr/api/1/datasets/?page=300'+str(i)+'&page_size=1'
		
		response = requests.get(urlString)
		responseContent = response.content
		returnDict[i] = responseContent
		
		
	return returnDict
	
'''
Given a mime value(from the resources mime keys) we get the file extension
HTML is represented by null so we return it directly
'''				
def getExtFromCT(mime):
	if "None" in mime:
		return "html"
	extension = mime.split("/")[1]
	if extension == "vnd.oasis.opendocument.spreadsheet":
		extension = "ods"
	return extension
	
'''
Given the previously returned dictionary from parseFunct(), we extract the corresponding values of the keys
title and latest, as well as the value of the field/key "mime", which helps
us identify the type of file found in the "latest" link
We save these values in a new dictionary returnDict

'''		
def getKeys(contentDict):
	returnDict = {}
	for i in range(0,len(contentDict)):
		dict = json.loads(contentDict[i])
		returnDict[i] = []#a list of dictionaries
		data = dict.get("data")
		
		for j in range(0, len(data[0].get("resources"))):
			returnDict[i].append({'title': str(data[0].get("resources")[j].get("title")),'latest':str(data[0].get("resources")[j].get("latest")),'ext':getExtFromCT(str(data[0].get("resources")[j].get("mime")).strip())})
			
			
	return returnDict

'''
create connection to database(and create database file if it doesn't exist)
'''	
def create_connection(dbFilePath):
	dbFilePath = dbFilePath+"\\"
	conn = None
	try:
		os.makedirs(dbFilePath)
	except OSError:
		pass
	conn = sqlite3.connect(dbFilePath+"\\simplePythonDatabase.db")
	return conn
'''
execute a given SQLite request
'''
def executeSqlReq(conn, tableReq):
	if conn is not None:
		cursor = conn.cursor()
		c = cursor.execute(tableReq)
		
		
'''
Once again, we make use of the dictionary returned in the function getKeys(..) to insert the different values
of title and latest, I also chose to insert the extension as it will be needed in the future when downloading files

'''
def addToTable(conn,dict):
	
	if conn is not None:
		for key in dict:
			for i in range(0,len(dict[key])):
				title = dict[key][i]['title']
				matchObj = re.match( r'(.+)\.[a-zA-Z]+$', title)
				if matchObj:
					title = matchObj.group(1)
				sqlReq = ' insert into OPENDATA values ("'+title+'", "'+dict[key][i]['latest']+'", "'+dict[key][i]['ext']+'")'
				cursor = conn.cursor()
				cursor.execute(sqlReq)
				conn.commit()


	
'''
We download the files here, storagePath is where we're planning to save the files
dict is the dictionary returned by getKeys(..)
'''	
def downloadLatestFiles(storagePath,dict):
	storagePath = storagePath +"\\"	
	try:
		os.makedirs(storagePath)
	except OSError:
		pass
	for filename in os.listdir(storagePath):
		try:
			os.remove(os.path.join(storagePath, filename))
		except:
			pass
	for elem in dict:
		for index in range(0,len(dict[elem])):
			currentUrl = dict[elem][index]['latest']
			fileName = dict[elem][index]['title']
			matchObj = re.match( r'(.+)\.[a-zA-Z]+$', fileName)

			if matchObj:
				fileName = matchObj.group(1)
			if fileName == "":
				fileName = "custom file name"+str(random.randint(0,1000))	
			fileName = fileName +"."+dict[elem][index]['ext']
			try:
				os.remove(fileName)
			except OSError:
				pass
			response = requests.get(currentUrl)
			with open(storagePath+fileName, 'wb') as results_file:
				results_file.write(response.content)
			

#main function
def main():
	dict = parseFunct()
	dict = getKeys(dict)
	conn = create_connection(".\\database")
	sqlRequest = """DROP TABLE IF EXISTS OPENDATA """
	executeSqlReq(conn,sqlRequest)
	sqlRequest = """ CREATE TABLE 	OPENDATA (
											title varchar(255),
											latest varchar(255),
											ext varchar(255)
										); 
										
				"""
	executeSqlReq(conn,sqlRequest)

	addToTable(conn,dict)


	downloadLatestFiles(".\\storage\\",dict)
	conn.commit()
	
	conn.close()
