import flask
from flask import render_template
from jinja2 import Template
from API_Miner import defParamFMD, main,defParamDSD
import json
from flask import jsonify
from flask import request,redirect
import re

app = flask.Flask(__name__)
app.config["DEBUG"] = True
main()
rep = defParamFMD() #json.loads(defParamFMD())

'''
If GET request is received, we return index.html, the jinja variables results, are set to be 
the meta data for every file/dataset, likewise, results is also used to add a corresponding button for each dataset

If POST request is received, a button was clicked, so we call defParamDSD to download the file after extracting
URL from the request
'''
@app.route('/', methods=['GET','POST'])
def home():
	if request.method == 'POST':
		fullResponse = request.form['url']
		
		matchObj = re.findall( r'http.+?"', fullResponse)

		
		defParamDSD(" ",matchObj[0].replace('"',""))
		return redirect("/")
	else:
		stt = json.loads(rep)
		res = []
		for dict in stt["metadata"]:
			res.append(json.dumps(dict, sort_keys = False, indent = 3))
		
		return render_template(
		'index.html',
		title='Home Page',
		results=res
		)
	
app.run()