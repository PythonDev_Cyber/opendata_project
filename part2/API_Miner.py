
import re
import requests
from part2 import create_connection,main as part2_main,getExtFromCT
import os.path
import json
import random
from tkinter import Tk
from tkinter.filedialog import askdirectory

'''this file represents our api'''

'''
Given a path for our database file, a table name and a storage path for the downloaded datasets,
we list the files name, latest link and extension(found in the table we created via the script part2.py)
We use the downloaded datasets to print the size, more useful properties can be found ; for example for csv/ods files : 
number and type of columns etc
We return the metadata found in json format
'''	
def listFilesMetaDeta(dataBasePath, tableName,storagePath):
	storagePath = storagePath +"\\"	
	
	metaDataDict = { }
	metaDataDict["metadata"] = []
	
	conn = create_connection(dataBasePath) 
	sqlRequest = '''select title,latest,ext from '''+tableName
	cursor = conn.cursor()
	cursor.execute(sqlRequest)
	 
	rows = cursor.fetchall()
	result = []
	for row in rows:
		appendDict = {"title":row[0],"latest":row[1],"extension":row[2]}
		  
		for file in os.listdir(storagePath):
			matchObj = re.match( r'(.+)\.([a-zA-Z]+)$', file)
			if matchObj:
					fileNameNoExt = matchObj.group(1)
					if row[0] ==fileNameNoExt and row[2]==matchObj.group(2):
						appendDict["fileSizeInBytes"] = str(os.path.getsize(os.path.join(storagePath,file)))
						
		metaDataDict["metadata"].append(appendDict)
		
	conn.close()
	return json.dumps(metaDataDict,sort_keys = True, indent = 4)

'''
Given a url and a storage path, we fetch the title of the file and extension then
we download the file
'''	
def downloadSpecificDataset(dataBasePath,tableName,storagePath,url):

	storagePath = storagePath +"\\"	
	conn = create_connection(dataBasePath) #".\\database"
	sqlRequest = '''select title,latest,ext from '''+tableName
	cursor = conn.cursor()
	cursor.execute(sqlRequest)
	
	rows = cursor.fetchall()
	for row in rows:
		if url in row[1]:	
			if row[0] == "":
				fileName = "custom file name"+str(random.randint(0,1000))
			else:
				fileName = row[0]
			fileName = fileName +"."+row[2]
			fullPath = storagePath+fileName
			try:
				os.remove(fullPath)
			except OSError:
				pass
			response = requests.get(row[1])
			with open(fullPath, 'wb') as results_file:
				results_file.write(response.content)
				
	
		
#call listFilesMetaDeta directly with out chosen parametres	
def defParamFMD():	
	return listFilesMetaDeta(".\\database","OPENDATA",".\\storage\\")

#call downloadSpecificDataset directly with out chosen parametres	
def defParamDSD(storagePath,url):
	root = Tk()
	storagePath = askdirectory(parent=root,initialdir="/",title='Select Folder')
	root.withdraw()


	downloadSpecificDataset(".\\database","OPENDATA",storagePath,url)
'''
part2.main() needs to be called once so that the database is created and the datasets are downloaded
'''	
def main():	
	part2_main()

	
