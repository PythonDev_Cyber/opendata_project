from part1 import parseFunct,getKeys,downloadLatestFiles
import pytest
import requests
import re
import os
import copy
dict = parseFunct()
resourcesDict = getKeys(dict)

resourcesDict2 = copy.deepcopy(resourcesDict)
'''
We are checking to see if our parseFunct returns the real content of each page
'''
def test_scraping():
	url0 = "https://www.data.gouv.fr/api/1/datasets/?page=3000&page_size=1"
	url1 = "https://www.data.gouv.fr/api/1/datasets/?page=3001&page_size=1"
	url2 = "https://www.data.gouv.fr/api/1/datasets/?page=3002&page_size=1"
	url3 = "https://www.data.gouv.fr/api/1/datasets/?page=3003&page_size=1"
	url4 = "https://www.data.gouv.fr/api/1/datasets/?page=3004&page_size=1"
	url5 = "https://www.data.gouv.fr/api/1/datasets/?page=3005&page_size=1"	
		
	
	
	assert dict[0] == requests.get(url0).content
	assert dict[1] == requests.get(url1).content
	assert dict[2] == requests.get(url2).content
	assert dict[3] == requests.get(url3).content
	assert dict[4] == requests.get(url4).content
	assert dict[5] == requests.get(url5).content

'''
the following tests are to check if we are getting the correct values of TITLE and LATEST for each page
'''
def test_keysFirstLink(): #https://www.data.gouv.fr/api/1/datasets/?page=3000&page_size=1
	link0 = [{"title": "","latest": "https://www.data.gouv.fr/fr/datasets/r/845d1ba8-29a4-4969-81bb-4159e5701508"}]
	print(resourcesDict[0])
	del resourcesDict[0][0]['ext']
	equal = True
	for elem in link0:
		if elem not in resourcesDict[0]:
			print(elem)
			equal = False
			break
	
	assert equal == True
	
def test_keysFirstLink():
	link0 = [{"title": "","latest": "https://www.data.gouv.fr/fr/datasets/r/845d1ba8-29a4-4969-81bb-4159e5701508"}]
	del resourcesDict[0][0]['ext']
	equal = True
	for elem in link0:
		if elem not in resourcesDict[0]:
			equal = False
			break
	
	assert equal == True
	
	
def test_keysSecondLink():#"https://www.data.gouv.fr/api/1/datasets/?page=3001&page_size=1"
	link1 = [{"title": "deliberation-commune-mogneneins-01252.csv","latest": "https://www.data.gouv.fr/fr/datasets/r/f32b773b-5044-4044-97ee-8c50fbbe7acc"}]
	del resourcesDict[1][0]['ext']
	equal = True
	for elem in link1:
		if elem not in resourcesDict[1]:
			equal = False
			break
	
	assert equal == True

def test_keysThirdLink(): #"https://www.data.gouv.fr/api/1/datasets/?page=3002&page_size=1"
	link2 = [{"title": "Budget primitif 2019 de la ville de Saint-Paul-l\u00e8s-Dax","latest": "https://www.data.gouv.fr/fr/datasets/r/610b4f17-ff10-4985-bad3-dc9f75554ee5"},
	{"title": "Budget primitif 2018 de la ville de Saint-Paul-l\u00e8s-Dax","latest": "https://www.data.gouv.fr/fr/datasets/r/2d975698-2856-44ae-947e-a8e072499fcf"},
	{"title": "Budget primitif 2017 de la ville de Saint-Paul-l\u00e8s-Dax","latest": "https://www.data.gouv.fr/fr/datasets/r/b4983d59-2f0e-402b-9e7c-2c976d365c8f"},
	{"title": "Document d\u00e9crivant le mod\u00e8le de donn\u00e9es","latest": "https://www.data.gouv.fr/fr/datasets/r/e1ea5eaf-b40b-43f4-98e7-7369f43fe9d3"}]
	
	for elem in resourcesDict[2]:
		del elem['ext']
	equal = True
	for elem in link2:
		if elem not in resourcesDict[2]:
			equal = False
			break
	
	assert equal == True	
	
def test_FourthLink():
	link3 = [{"title": "20200214-plan-ville-equipements-voies.zip","latest": "https://www.data.gouv.fr/fr/datasets/r/79ca9f10-dae9-4e11-a51e-a2e4a1215367"},{"title": "20200214-plan-ville-equipements-voies.png","latest": "https://www.data.gouv.fr/fr/datasets/r/bc13d1be-9308-4a15-ac86-9afed9ba2d6d"},{"title": "20200214-plan-ville-equipements-voies.pdf","latest": "https://www.data.gouv.fr/fr/datasets/r/c1fb38f2-f485-4f47-b9c9-119e45f69457"}]
	
	
	for elem in resourcesDict[3]:
		del elem['ext']
	equal = True
	for elem in link3:
		if elem not in resourcesDict[3]:
			equal = False
			break
	
	assert equal == True	
def test_FifthLink():
	link4 = [{"title": "l-bilan-fonds-barnier-t-069-2017.ods","latest": "https://www.data.gouv.fr/fr/datasets/r/24cbba45-ce53-4500-9e6f-421bb60e2821"},{"title": "l-bilan-fonds-barnier-t-069-2018.ods","latest": "https://www.data.gouv.fr/fr/datasets/r/65240d2e-b24d-4507-9a8c-febbfc735a0b"},{"title": "l-bilan-fonds-barnier-t-069-2019.ods","latest": "https://www.data.gouv.fr/fr/datasets/r/754585ff-8d7f-4522-9916-140b25e79b98"}]
	
	
	for elem in resourcesDict[4]:
		del elem['ext']
	equal = True
	for elem in link4:
		if elem not in resourcesDict[4]:
			equal = False
			break
	
	assert equal == True	
def test_SixthLink():
	
	link5= [{"title": "Liste des CEE","latest": "https://www.data.gouv.fr/fr/datasets/r/09eb664f-b121-485f-9e49-70bd020eea33"}]
	
	for elem in resourcesDict[5]:
		del elem['ext']
	equal = True
	for elem in link5:
		if elem not in resourcesDict[5]:
			equal = False
			break
	
	assert equal == True	
	
'''
checking download (verify if files have been downloaded
delete all files then begin download and check if number of files downloaded is the same as the number of title-latest
entries in the dictionary
'''
def test_downloadFilesTest():
	
	resourcesDict= resourcesDict2
	storagePath = ".\\storage\\"
	try:
		os.makedirs(storagePath)
	except OSError:
		pass
	for filename in os.listdir(storagePath):
		try:
			os.remove(os.path.join(storagePath, filename))
		except:
			pass
	downloadLatestFiles(storagePath,resourcesDict)
	count = 0
	numberElem = 0

	for filename in os.listdir(storagePath):
		count = count +1
		
	for index in range(0,len(resourcesDict)):
		numberElem = numberElem + len(resourcesDict[index])
	assert count == numberElem
	